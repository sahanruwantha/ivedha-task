from flask import Flask, request, jsonify
from elasticsearch import Elasticsearch

app = Flask(__name__)
es = Elasticsearch(['http://localhost:9200'])  # Establish connection to Elasticsearch, assuming it's running locally

# Endpoint to insert payload into Elasticsearch
@app.route('/add', methods=['POST'])
def add_to_elasticsearch():
    data = request.get_json()

    # Assuming 'index_name' is the index where you want to store the data
    index_name = "your_index_name"

    # Insert data into Elasticsearch
    es.index(index=index_name, body=data)

    return jsonify({"message": "Data added to Elasticsearch successfully"})

# Endpoint to get application status
@app.route('/healthcheck', methods=['GET'])
def get_application_status():
    # Assuming 'index_name' is the index where application status is stored
    index_name = "your_application_status_index"

    # Fetch overall application status from Elasticsearch
    result = es.search(index=index_name, body={"query": {"match_all": {}}})

    return jsonify({"application_status": result})  # Modify this to return the relevant status

# Endpoint to get specific service status
@app.route('/healthcheck/<service_name>', methods=['GET'])
def get_specific_service_status(service_name):
    # Assuming 'index_name' is the index where service statuses are stored
    index_name = "your_service_status_index"

    # Fetch specific service status from Elasticsearch based on service_name
    result = es.search(index=index_name, body={"query": {"match": {"service_name": service_name}}})

    return jsonify({f"{service_name}_status": result})  # Modify this to return the relevant status

if __name__ == '__main__':
    app.run(debug=True)  # Change to False in production
