import json
import os
import subprocess
from datetime import datetime

services = ['httpd', 'rabbitMQ', 'postgreSQL']
host_name = os.uname().nodename
timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

for service in services:
    service_status = "UP"
    cmd = f"systemctl is-active {service}.service"
    try:
        subprocess.run(cmd.split(), check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError:
        service_status = "DOWN"

    json_payload = {
        "service_name": service,
        "service_status": service_status,
        "host_name": host_name
    }

    file_name = f"{service}-status-{timestamp}.json"
    with open(file_name, 'w') as file:
        json.dump(json_payload, file)
