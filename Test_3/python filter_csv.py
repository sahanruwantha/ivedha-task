import pandas as pd

# Read CSV file
df = pd.read_csv('sales-data.csv')

# Calculate average price per foot
avg_price_per_foot = df['Sale Price'] / df['Square Footage']
average_price = avg_price_per_foot.mean()

# Filter properties sold for less than average price per foot
filtered_data = df[avg_price_per_foot < average_price]

# Save filtered data to a new CSV file
filtered_data.to_csv('filtered-sales-data.csv', index=False)
