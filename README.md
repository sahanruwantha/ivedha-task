# Assignment README

## Test 1:

### Monitoring Services and Creating JSON Objects
- The Python script `monitor_services.py` monitors the status of critical services (`httpd`, `rabbitMQ`, `postgreSQL`) and creates JSON objects containing service status, service name, and host name.
  - Ensure Python 3.x is installed.
  - Run the script using the command: `python monitor_services.py`.

### Python REST Webservice for Elasticsearch Interaction
- The Flask-based Python REST service interacts with Elasticsearch to manage application status and service statuses.
  - Ensure Flask (`pip install flask`) and required dependencies are installed.
  - Update the Flask app (`rest_service.py`) to connect to your Elasticsearch instance by setting the correct Elasticsearch URL in the code.
  - Run the Flask app using the command: `python rest_service.py`.
  - Endpoints:
    - POST `/add`: Adds data to Elasticsearch.
    - GET `/healthcheck`: Returns overall application status.
    - GET `/healthcheck/<service_name>`: Returns specific service status.

## Test 2:

### Ansible Playbook for Service Management
- An Ansible playbook `assignment.yml` is included to manage service installations, check disk space, and fetch application/service status.
  - Ensure Ansible is installed (`pip install ansible`).
  - Adjust the Ansible inventory file (`inventory`) to reflect the correct hosts and groupings.
  - Run the playbook using the command: `ansible-playbook assignment.yml -i inventory -e action=<desired_action>`.
  - Options for `action`:
    - `verify_install`: Verify or install services.
    - `check-disk`: Check disk space and send alerts if usage is >80%.
    - `check-status`: Fetch application status and down services.

## Test 3:

### Filtering CSV Data
- The Python script `filter_csv.py` filters a CSV file based on property sale prices.
  - Ensure Pandas (`pip install pandas`) is installed.
  - Place the input CSV file (`sales-data.csv`) in the same directory.
  - Run the script using the command: `python filter_csv.py`.
  - Output: `filtered-sales-data.csv` containing properties sold for less than the average price per foot.

---

Note: Replace placeholders, configure necessary dependencies, set Elasticsearch connection details, and adjust paths according to your specific setup/environment before executing the scripts or playbooks.
